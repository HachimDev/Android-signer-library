
import { CreateKeyStoreCommand } from './commands/createKeyStoreCommand';
import {SignApkCommand} from './commands/signApk.command';

require("yargs")
    .usage("Usage: $0 <command> [options]")
    .command(new CreateKeyStoreCommand())
    .command(new SignApkCommand())
    .demand(1)
    .version(() => require("./package.json").version)
    .alias("v", "version")
    .help("h")
    .alias("h", "help")
    .argv;



    