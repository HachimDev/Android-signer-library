import * as cp from 'child_process';
import * as path from 'path';

/** Options that can be passed to execTask or execNodeTask. */
export interface ExecTaskOptions {
    // Whether to output to STDERR and STDOUT.
    silent?: boolean;
    // If an error happens, this will replace the standard error.
    errMessage?: string;
}

/** Create a task that executes a binary as if from the command line. */
export function execTask(binPath: string, args: string[], options: ExecTaskOptions = {}) {
    return new Promise((resolve, reject) => {
        const childProcess = cp.spawn(binPath, args);

        if (!options.silent) {
            childProcess.stdout.on('data', (data: string) => {
                process.stdout.write(data);
            });

            childProcess.stderr.on('data', (data: string) => {
                process.stderr.write(data);
            });
        }

        childProcess.on('close', (code: number) => {
            if (code != 0) {

                if (options.errMessage === undefined) {
                    reject('Process failed with code ' + code);
                } else {
                    reject(options.errMessage);
                }
            } else {
                resolve();
            }
        });
    });
}




export const SHELL_DIR = path.join(__dirname, '..', '..', '..',  'scripts');
export const SIGNER_DIR = path.join(__dirname, '..', '..', '..');
