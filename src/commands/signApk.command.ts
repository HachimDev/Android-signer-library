const chalk = require("chalk");
import { execTask, SHELL_DIR, SIGNER_DIR } from '../utils/utils';
import * as path from 'path';


export class SignApkCommand {
    command = "sign:apk";
    describe = "sign the apk with the keystore";




    builder(yargs: any) {
        return yargs
            .option("ks", {
                alias: "ksName",
                default: "default"
            })
            .option("al", {
                alias: "aliasName",
                default: "default"
            })
            .option("pwd", {
                alias: "password",
                default: "default"
            })
            .option("aP", {
                alias:"apkPath",
                default: "default"
            });
    }

    // async handler(argv: any) {
    //     try {
    //         const args: string[] = [];
    //         const shellPath = path.join(SHELL_DIR, 'sign-apk.sh');
    //         await execTask('sh', [shellPath, ...args]);
    //     } catch (error) {
    //         console.error(error);

    //     }
    // }

    async handler(argv: any) {
        try {
            const shellPath = path.join(SHELL_DIR, 'sign-apk.sh');
            await execTask(shellPath, [argv.ksName, argv.aliasName, argv.password, argv.apkPath, SIGNER_DIR]);
        } catch (error) {
            console.error(error);
        }
    }
}