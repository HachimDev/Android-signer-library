const chalk = require("chalk");
import { execTask, SHELL_DIR } from '../utils/utils';
import * as path from 'path';


export class CreateKeyStoreCommand {
    command = "keystore:create";
    describe = "generate a keyStore";




    builder(yargs: any) {
        return yargs
            .option("ks", {
                alias: "ksName",
                default: "default"
            })
            .option("al", {
                alias: "aliasName",
                default: "mainKeyStore"
            })
            .option("pwd", {
                alias: "password",
                default: "azerty12"
            });
    }


    // async handler(argv: any) {
    //     try {
    //         const args: string[] = [];
    //         const shellPath = path.join(SHELL_DIR, 'sign-apk.sh');
    //         await execTask('sh', [shellPath, ...args]);
    //     } catch (error) {
    //         console.error(error);

    //     }
    // }

    async handler(argv: any) {
        try {
            const shellPath = path.join(SHELL_DIR, 'createKeyStore.sh');
            await execTask(shellPath, [argv.ksName, argv.aliasName, argv.password]);
        } catch (error) {
            console.error(error);

        }
    }
}