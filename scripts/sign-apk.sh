#!/bin/sh
set -e


echo 'BEGINNING'

if [ -e ./platforms/android/build/output/apk/Signer_Storage/$1 ]
then
    echo 'keystore found in Signer Storage'
    if [ -e $5/uber-apk-signer.jar ]
    then
        echo '\n=============== starting the Uber Signer ===============\n'
        java -jar $5/uber-apk-signer.jar -a ./platforms/android/build/outputs/apk/ --out $4 --ks ./platforms/android/build/output/apk/Signer_Storage/$1 --ksAlias $2 --ksKeyPass $3 --ksPass $3
    else
        echo 'uber-apk-signer.jar missing'
        exit 1
    fi
else 
    echo 'keystore missing, use "sign keystore:create" to create a new one'
    exit 1
fi

echo 'END'