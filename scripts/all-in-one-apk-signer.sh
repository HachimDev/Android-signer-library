#!/bin/sh


# 1st param $1  = keystore name
# 2nd param $2 = password for the key

echo Signing apk Script


# Generate the apk if possible
echo 'Phase #1 : APK'

if [ ! -e ./android-release-unsigned.apk ]
then
echo 'apk not found, Generating it...'
    npm run android:release #generate the APK
else
    echo 'android-release-unsigned.apk already existing. Moving on...'
fi
# ==================================


# creating the keystore.keystore 
    echo '\n=============== Starting the keyStore generation  ===============\n'

keytool -genkey -v -keystore $1 -alias mainKeyStore -keyalg RSA -keysize 2048 -validity 10000 \
            -dname "CN=Hachim OU=magnus 0=SUN L=Casablanca S=maroc C=maroc" \
            -storepass $2 -keypass $2

# ==================================


# Sign the generated apk with the generated keystore to generate the signed apk

if [ -e ./uber-apk-signer.jar ]
then
    echo '\n=============== starting the Uber Signer ===============\n'
    java -jar uber-apk-signer.jar -a ./ --out signedApk/ --ks $1 --ksAlias mainKeyStore --ksKeyPass $2 --ksPass $2
    # java -jar uber-apk-signer.jar -a ./ --onlyVerify #Verify the apk 

else
    echo 'uber-apk-signer.jar missing'
fi

# ==================================


echo 'Script Ended..'