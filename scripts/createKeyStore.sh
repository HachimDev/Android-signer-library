#!/bin/sh


keytool -genkey -v -keystore $1 -alias $2 -keyalg RSA -keysize 2048 -validity 10000 \
            -dname "CN=Hachim OU=magnus 0=SUN L=Casablanca S=maroc C=maroc" \
            -storepass $3 -keypass $3

mkdir -p platforms/android/build/output/apk/Signer_Storage/
mv k.keystore platforms/android/build/output/apk/Signer_Storage/

echo 'You have created a new keystore : \n'
echo 'keyStore Name : '$1
echo 'Alias Name : '$2