/** Options that can be passed to execTask or execNodeTask. */
export interface ExecTaskOptions {
    silent?: boolean;
    errMessage?: string;
}
/** Create a task that executes a binary as if from the command line. */
export declare function execTask(binPath: string, args: string[], options?: ExecTaskOptions): Promise<{}>;
export declare const SHELL_DIR: string;
export declare const SIGNER_DIR: string;
