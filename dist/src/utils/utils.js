"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var cp = require("child_process");
var path = require("path");
/** Create a task that executes a binary as if from the command line. */
function execTask(binPath, args, options) {
    if (options === void 0) { options = {}; }
    return new Promise(function (resolve, reject) {
        var childProcess = cp.spawn(binPath, args);
        if (!options.silent) {
            childProcess.stdout.on('data', function (data) {
                process.stdout.write(data);
            });
            childProcess.stderr.on('data', function (data) {
                process.stderr.write(data);
            });
        }
        childProcess.on('close', function (code) {
            if (code != 0) {
                if (options.errMessage === undefined) {
                    reject('Process failed with code ' + code);
                }
                else {
                    reject(options.errMessage);
                }
            }
            else {
                resolve();
            }
        });
    });
}
exports.execTask = execTask;
exports.SHELL_DIR = path.join(__dirname, '..', '..', '..', 'scripts');
exports.SIGNER_DIR = path.join(__dirname, '..', '..', '..');
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXRpbHMuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi9zcmMvdXRpbHMvdXRpbHMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxrQ0FBb0M7QUFDcEMsMkJBQTZCO0FBVTdCLHdFQUF3RTtBQUN4RSxrQkFBeUIsT0FBZSxFQUFFLElBQWMsRUFBRSxPQUE2QjtJQUE3Qix3QkFBQSxFQUFBLFlBQTZCO0lBQ25GLE1BQU0sQ0FBQyxJQUFJLE9BQU8sQ0FBQyxVQUFDLE9BQU8sRUFBRSxNQUFNO1FBQy9CLElBQU0sWUFBWSxHQUFHLEVBQUUsQ0FBQyxLQUFLLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBRTdDLEVBQUUsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFDbEIsWUFBWSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsTUFBTSxFQUFFLFVBQUMsSUFBWTtnQkFDeEMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDL0IsQ0FBQyxDQUFDLENBQUM7WUFFSCxZQUFZLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxNQUFNLEVBQUUsVUFBQyxJQUFZO2dCQUN4QyxPQUFPLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUMvQixDQUFDLENBQUMsQ0FBQztRQUNQLENBQUM7UUFFRCxZQUFZLENBQUMsRUFBRSxDQUFDLE9BQU8sRUFBRSxVQUFDLElBQVk7WUFDbEMsRUFBRSxDQUFDLENBQUMsSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBRVosRUFBRSxDQUFDLENBQUMsT0FBTyxDQUFDLFVBQVUsS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDO29CQUNuQyxNQUFNLENBQUMsMkJBQTJCLEdBQUcsSUFBSSxDQUFDLENBQUM7Z0JBQy9DLENBQUM7Z0JBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ0osTUFBTSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQztnQkFDL0IsQ0FBQztZQUNMLENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDSixPQUFPLEVBQUUsQ0FBQztZQUNkLENBQUM7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUMsQ0FBQyxDQUFDO0FBQ1AsQ0FBQztBQTNCRCw0QkEyQkM7QUFLWSxRQUFBLFNBQVMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRyxTQUFTLENBQUMsQ0FBQztBQUMvRCxRQUFBLFVBQVUsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0ICogYXMgY3AgZnJvbSAnY2hpbGRfcHJvY2Vzcyc7XG5pbXBvcnQgKiBhcyBwYXRoIGZyb20gJ3BhdGgnO1xuXG4vKiogT3B0aW9ucyB0aGF0IGNhbiBiZSBwYXNzZWQgdG8gZXhlY1Rhc2sgb3IgZXhlY05vZGVUYXNrLiAqL1xuZXhwb3J0IGludGVyZmFjZSBFeGVjVGFza09wdGlvbnMge1xuICAgIC8vIFdoZXRoZXIgdG8gb3V0cHV0IHRvIFNUREVSUiBhbmQgU1RET1VULlxuICAgIHNpbGVudD86IGJvb2xlYW47XG4gICAgLy8gSWYgYW4gZXJyb3IgaGFwcGVucywgdGhpcyB3aWxsIHJlcGxhY2UgdGhlIHN0YW5kYXJkIGVycm9yLlxuICAgIGVyck1lc3NhZ2U/OiBzdHJpbmc7XG59XG5cbi8qKiBDcmVhdGUgYSB0YXNrIHRoYXQgZXhlY3V0ZXMgYSBiaW5hcnkgYXMgaWYgZnJvbSB0aGUgY29tbWFuZCBsaW5lLiAqL1xuZXhwb3J0IGZ1bmN0aW9uIGV4ZWNUYXNrKGJpblBhdGg6IHN0cmluZywgYXJnczogc3RyaW5nW10sIG9wdGlvbnM6IEV4ZWNUYXNrT3B0aW9ucyA9IHt9KSB7XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICAgICAgY29uc3QgY2hpbGRQcm9jZXNzID0gY3Auc3Bhd24oYmluUGF0aCwgYXJncyk7XG5cbiAgICAgICAgaWYgKCFvcHRpb25zLnNpbGVudCkge1xuICAgICAgICAgICAgY2hpbGRQcm9jZXNzLnN0ZG91dC5vbignZGF0YScsIChkYXRhOiBzdHJpbmcpID0+IHtcbiAgICAgICAgICAgICAgICBwcm9jZXNzLnN0ZG91dC53cml0ZShkYXRhKTtcbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICBjaGlsZFByb2Nlc3Muc3RkZXJyLm9uKCdkYXRhJywgKGRhdGE6IHN0cmluZykgPT4ge1xuICAgICAgICAgICAgICAgIHByb2Nlc3Muc3RkZXJyLndyaXRlKGRhdGEpO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cblxuICAgICAgICBjaGlsZFByb2Nlc3Mub24oJ2Nsb3NlJywgKGNvZGU6IG51bWJlcikgPT4ge1xuICAgICAgICAgICAgaWYgKGNvZGUgIT0gMCkge1xuXG4gICAgICAgICAgICAgICAgaWYgKG9wdGlvbnMuZXJyTWVzc2FnZSA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICAgICAgICAgIHJlamVjdCgnUHJvY2VzcyBmYWlsZWQgd2l0aCBjb2RlICcgKyBjb2RlKTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICByZWplY3Qob3B0aW9ucy5lcnJNZXNzYWdlKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHJlc29sdmUoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgfSk7XG59XG5cblxuXG5cbmV4cG9ydCBjb25zdCBTSEVMTF9ESVIgPSBwYXRoLmpvaW4oX19kaXJuYW1lLCAnLi4nLCAnLi4nLCAnLi4nLCAgJ3NjcmlwdHMnKTtcbmV4cG9ydCBjb25zdCBTSUdORVJfRElSID0gcGF0aC5qb2luKF9fZGlybmFtZSwgJy4uJywgJy4uJywgJy4uJyk7XG4iXX0=