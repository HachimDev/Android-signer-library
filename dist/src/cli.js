"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var createKeyStoreCommand_1 = require("./commands/createKeyStoreCommand");
var signApk_command_1 = require("./commands/signApk.command");
require("yargs")
    .usage("Usage: $0 <command> [options]")
    .command(new createKeyStoreCommand_1.CreateKeyStoreCommand())
    .command(new signApk_command_1.SignApkCommand())
    .demand(1)
    .version(function () { return require("./package.json").version; })
    .alias("v", "version")
    .help("h")
    .alias("h", "help")
    .argv;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2xpLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vc3JjL2NsaS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLDBFQUF5RTtBQUN6RSw4REFBMEQ7QUFFMUQsT0FBTyxDQUFDLE9BQU8sQ0FBQztLQUNYLEtBQUssQ0FBQywrQkFBK0IsQ0FBQztLQUN0QyxPQUFPLENBQUMsSUFBSSw2Q0FBcUIsRUFBRSxDQUFDO0tBQ3BDLE9BQU8sQ0FBQyxJQUFJLGdDQUFjLEVBQUUsQ0FBQztLQUM3QixNQUFNLENBQUMsQ0FBQyxDQUFDO0tBQ1QsT0FBTyxDQUFDLGNBQU0sT0FBQSxPQUFPLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxPQUFPLEVBQWpDLENBQWlDLENBQUM7S0FDaEQsS0FBSyxDQUFDLEdBQUcsRUFBRSxTQUFTLENBQUM7S0FDckIsSUFBSSxDQUFDLEdBQUcsQ0FBQztLQUNULEtBQUssQ0FBQyxHQUFHLEVBQUUsTUFBTSxDQUFDO0tBQ2xCLElBQUksQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbIlxuaW1wb3J0IHsgQ3JlYXRlS2V5U3RvcmVDb21tYW5kIH0gZnJvbSAnLi9jb21tYW5kcy9jcmVhdGVLZXlTdG9yZUNvbW1hbmQnO1xuaW1wb3J0IHtTaWduQXBrQ29tbWFuZH0gZnJvbSAnLi9jb21tYW5kcy9zaWduQXBrLmNvbW1hbmQnO1xuXG5yZXF1aXJlKFwieWFyZ3NcIilcbiAgICAudXNhZ2UoXCJVc2FnZTogJDAgPGNvbW1hbmQ+IFtvcHRpb25zXVwiKVxuICAgIC5jb21tYW5kKG5ldyBDcmVhdGVLZXlTdG9yZUNvbW1hbmQoKSlcbiAgICAuY29tbWFuZChuZXcgU2lnbkFwa0NvbW1hbmQoKSlcbiAgICAuZGVtYW5kKDEpXG4gICAgLnZlcnNpb24oKCkgPT4gcmVxdWlyZShcIi4vcGFja2FnZS5qc29uXCIpLnZlcnNpb24pXG4gICAgLmFsaWFzKFwidlwiLCBcInZlcnNpb25cIilcbiAgICAuaGVscChcImhcIilcbiAgICAuYWxpYXMoXCJoXCIsIFwiaGVscFwiKVxuICAgIC5hcmd2O1xuXG5cblxuICAgICJdfQ==