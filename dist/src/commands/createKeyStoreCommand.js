"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var chalk = require("chalk");
var utils_1 = require("../utils/utils");
var path = require("path");
var CreateKeyStoreCommand = (function () {
    function CreateKeyStoreCommand() {
        this.command = "keystore:create";
        this.describe = "generate a keyStore";
    }
    CreateKeyStoreCommand.prototype.builder = function (yargs) {
        return yargs
            .option("ks", {
            alias: "ksName",
            default: "default"
        })
            .option("al", {
            alias: "aliasName",
            default: "mainKeyStore"
        })
            .option("pwd", {
            alias: "password",
            default: "azerty12"
        });
    };
    // async handler(argv: any) {
    //     try {
    //         const args: string[] = [];
    //         const shellPath = path.join(SHELL_DIR, 'sign-apk.sh');
    //         await execTask('sh', [shellPath, ...args]);
    //     } catch (error) {
    //         console.error(error);
    //     }
    // }
    CreateKeyStoreCommand.prototype.handler = function (argv) {
        return __awaiter(this, void 0, void 0, function () {
            var shellPath, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        shellPath = path.join(utils_1.SHELL_DIR, 'createKeyStore.sh');
                        return [4 /*yield*/, utils_1.execTask(shellPath, [argv.ksName, argv.aliasName, argv.password])];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        error_1 = _a.sent();
                        console.error(error_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    return CreateKeyStoreCommand;
}());
exports.CreateKeyStoreCommand = CreateKeyStoreCommand;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3JlYXRlS2V5U3RvcmVDb21tYW5kLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL2NvbW1hbmRzL2NyZWF0ZUtleVN0b3JlQ29tbWFuZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsSUFBTSxLQUFLLEdBQUcsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDO0FBQy9CLHdDQUFxRDtBQUNyRCwyQkFBNkI7QUFHN0I7SUFBQTtRQUNJLFlBQU8sR0FBRyxpQkFBaUIsQ0FBQztRQUM1QixhQUFRLEdBQUcscUJBQXFCLENBQUM7SUEwQ3JDLENBQUM7SUFyQ0csdUNBQU8sR0FBUCxVQUFRLEtBQVU7UUFDZCxNQUFNLENBQUMsS0FBSzthQUNQLE1BQU0sQ0FBQyxJQUFJLEVBQUU7WUFDVixLQUFLLEVBQUUsUUFBUTtZQUNmLE9BQU8sRUFBRSxTQUFTO1NBQ3JCLENBQUM7YUFDRCxNQUFNLENBQUMsSUFBSSxFQUFFO1lBQ1YsS0FBSyxFQUFFLFdBQVc7WUFDbEIsT0FBTyxFQUFFLGNBQWM7U0FDMUIsQ0FBQzthQUNELE1BQU0sQ0FBQyxLQUFLLEVBQUU7WUFDWCxLQUFLLEVBQUUsVUFBVTtZQUNqQixPQUFPLEVBQUUsVUFBVTtTQUN0QixDQUFDLENBQUM7SUFDWCxDQUFDO0lBR0QsNkJBQTZCO0lBQzdCLFlBQVk7SUFDWixxQ0FBcUM7SUFDckMsaUVBQWlFO0lBQ2pFLHNEQUFzRDtJQUN0RCx3QkFBd0I7SUFDeEIsZ0NBQWdDO0lBRWhDLFFBQVE7SUFDUixJQUFJO0lBRUUsdUNBQU8sR0FBYixVQUFjLElBQVM7Ozs7Ozs7d0JBRVQsU0FBUyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsaUJBQVMsRUFBRSxtQkFBbUIsQ0FBQyxDQUFDO3dCQUM1RCxxQkFBTSxnQkFBUSxDQUFDLFNBQVMsRUFBRSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsRUFBQTs7d0JBQXZFLFNBQXVFLENBQUM7Ozs7d0JBRXhFLE9BQU8sQ0FBQyxLQUFLLENBQUMsT0FBSyxDQUFDLENBQUM7Ozs7OztLQUc1QjtJQUNMLDRCQUFDO0FBQUQsQ0FBQyxBQTVDRCxJQTRDQztBQTVDWSxzREFBcUIiLCJzb3VyY2VzQ29udGVudCI6WyJjb25zdCBjaGFsayA9IHJlcXVpcmUoXCJjaGFsa1wiKTtcbmltcG9ydCB7IGV4ZWNUYXNrLCBTSEVMTF9ESVIgfSBmcm9tICcuLi91dGlscy91dGlscyc7XG5pbXBvcnQgKiBhcyBwYXRoIGZyb20gJ3BhdGgnO1xuXG5cbmV4cG9ydCBjbGFzcyBDcmVhdGVLZXlTdG9yZUNvbW1hbmQge1xuICAgIGNvbW1hbmQgPSBcImtleXN0b3JlOmNyZWF0ZVwiO1xuICAgIGRlc2NyaWJlID0gXCJnZW5lcmF0ZSBhIGtleVN0b3JlXCI7XG5cblxuXG5cbiAgICBidWlsZGVyKHlhcmdzOiBhbnkpIHtcbiAgICAgICAgcmV0dXJuIHlhcmdzXG4gICAgICAgICAgICAub3B0aW9uKFwia3NcIiwge1xuICAgICAgICAgICAgICAgIGFsaWFzOiBcImtzTmFtZVwiLFxuICAgICAgICAgICAgICAgIGRlZmF1bHQ6IFwiZGVmYXVsdFwiXG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgLm9wdGlvbihcImFsXCIsIHtcbiAgICAgICAgICAgICAgICBhbGlhczogXCJhbGlhc05hbWVcIixcbiAgICAgICAgICAgICAgICBkZWZhdWx0OiBcIm1haW5LZXlTdG9yZVwiXG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgLm9wdGlvbihcInB3ZFwiLCB7XG4gICAgICAgICAgICAgICAgYWxpYXM6IFwicGFzc3dvcmRcIixcbiAgICAgICAgICAgICAgICBkZWZhdWx0OiBcImF6ZXJ0eTEyXCJcbiAgICAgICAgICAgIH0pO1xuICAgIH1cblxuXG4gICAgLy8gYXN5bmMgaGFuZGxlcihhcmd2OiBhbnkpIHtcbiAgICAvLyAgICAgdHJ5IHtcbiAgICAvLyAgICAgICAgIGNvbnN0IGFyZ3M6IHN0cmluZ1tdID0gW107XG4gICAgLy8gICAgICAgICBjb25zdCBzaGVsbFBhdGggPSBwYXRoLmpvaW4oU0hFTExfRElSLCAnc2lnbi1hcGsuc2gnKTtcbiAgICAvLyAgICAgICAgIGF3YWl0IGV4ZWNUYXNrKCdzaCcsIFtzaGVsbFBhdGgsIC4uLmFyZ3NdKTtcbiAgICAvLyAgICAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICAvLyAgICAgICAgIGNvbnNvbGUuZXJyb3IoZXJyb3IpO1xuXG4gICAgLy8gICAgIH1cbiAgICAvLyB9XG5cbiAgICBhc3luYyBoYW5kbGVyKGFyZ3Y6IGFueSkge1xuICAgICAgICB0cnkge1xuICAgICAgICAgICAgY29uc3Qgc2hlbGxQYXRoID0gcGF0aC5qb2luKFNIRUxMX0RJUiwgJ2NyZWF0ZUtleVN0b3JlLnNoJyk7XG4gICAgICAgICAgICBhd2FpdCBleGVjVGFzayhzaGVsbFBhdGgsIFthcmd2LmtzTmFtZSwgYXJndi5hbGlhc05hbWUsIGFyZ3YucGFzc3dvcmRdKTtcbiAgICAgICAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoZXJyb3IpO1xuXG4gICAgICAgIH1cbiAgICB9XG59Il19