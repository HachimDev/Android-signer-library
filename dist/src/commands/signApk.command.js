"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var chalk = require("chalk");
var utils_1 = require("../utils/utils");
var path = require("path");
var SignApkCommand = (function () {
    function SignApkCommand() {
        this.command = "sign:apk";
        this.describe = "sign the apk with the keystore";
    }
    SignApkCommand.prototype.builder = function (yargs) {
        return yargs
            .option("ks", {
            alias: "ksName",
            default: "default"
        })
            .option("al", {
            alias: "aliasName",
            default: "default"
        })
            .option("pwd", {
            alias: "password",
            default: "default"
        })
            .option("aP", {
            alias: "apkPath",
            default: "default"
        });
    };
    // async handler(argv: any) {
    //     try {
    //         const args: string[] = [];
    //         const shellPath = path.join(SHELL_DIR, 'sign-apk.sh');
    //         await execTask('sh', [shellPath, ...args]);
    //     } catch (error) {
    //         console.error(error);
    //     }
    // }
    SignApkCommand.prototype.handler = function (argv) {
        return __awaiter(this, void 0, void 0, function () {
            var shellPath, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        shellPath = path.join(utils_1.SHELL_DIR, 'sign-apk.sh');
                        return [4 /*yield*/, utils_1.execTask(shellPath, [argv.ksName, argv.aliasName, argv.password, argv.apkPath, utils_1.SIGNER_DIR])];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        error_1 = _a.sent();
                        console.error(error_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    return SignApkCommand;
}());
exports.SignApkCommand = SignApkCommand;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2lnbkFway5jb21tYW5kLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL2NvbW1hbmRzL3NpZ25BcGsuY29tbWFuZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsSUFBTSxLQUFLLEdBQUcsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDO0FBQy9CLHdDQUFpRTtBQUNqRSwyQkFBNkI7QUFHN0I7SUFBQTtRQUNJLFlBQU8sR0FBRyxVQUFVLENBQUM7UUFDckIsYUFBUSxHQUFHLGdDQUFnQyxDQUFDO0lBNENoRCxDQUFDO0lBdkNHLGdDQUFPLEdBQVAsVUFBUSxLQUFVO1FBQ2QsTUFBTSxDQUFDLEtBQUs7YUFDUCxNQUFNLENBQUMsSUFBSSxFQUFFO1lBQ1YsS0FBSyxFQUFFLFFBQVE7WUFDZixPQUFPLEVBQUUsU0FBUztTQUNyQixDQUFDO2FBQ0QsTUFBTSxDQUFDLElBQUksRUFBRTtZQUNWLEtBQUssRUFBRSxXQUFXO1lBQ2xCLE9BQU8sRUFBRSxTQUFTO1NBQ3JCLENBQUM7YUFDRCxNQUFNLENBQUMsS0FBSyxFQUFFO1lBQ1gsS0FBSyxFQUFFLFVBQVU7WUFDakIsT0FBTyxFQUFFLFNBQVM7U0FDckIsQ0FBQzthQUNELE1BQU0sQ0FBQyxJQUFJLEVBQUU7WUFDVixLQUFLLEVBQUMsU0FBUztZQUNmLE9BQU8sRUFBRSxTQUFTO1NBQ3JCLENBQUMsQ0FBQztJQUNYLENBQUM7SUFFRCw2QkFBNkI7SUFDN0IsWUFBWTtJQUNaLHFDQUFxQztJQUNyQyxpRUFBaUU7SUFDakUsc0RBQXNEO0lBQ3RELHdCQUF3QjtJQUN4QixnQ0FBZ0M7SUFFaEMsUUFBUTtJQUNSLElBQUk7SUFFRSxnQ0FBTyxHQUFiLFVBQWMsSUFBUzs7Ozs7Ozt3QkFFVCxTQUFTLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxpQkFBUyxFQUFFLGFBQWEsQ0FBQyxDQUFDO3dCQUN0RCxxQkFBTSxnQkFBUSxDQUFDLFNBQVMsRUFBRSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxPQUFPLEVBQUUsa0JBQVUsQ0FBQyxDQUFDLEVBQUE7O3dCQUFqRyxTQUFpRyxDQUFDOzs7O3dCQUVsRyxPQUFPLENBQUMsS0FBSyxDQUFDLE9BQUssQ0FBQyxDQUFDOzs7Ozs7S0FFNUI7SUFDTCxxQkFBQztBQUFELENBQUMsQUE5Q0QsSUE4Q0M7QUE5Q1ksd0NBQWMiLCJzb3VyY2VzQ29udGVudCI6WyJjb25zdCBjaGFsayA9IHJlcXVpcmUoXCJjaGFsa1wiKTtcbmltcG9ydCB7IGV4ZWNUYXNrLCBTSEVMTF9ESVIsIFNJR05FUl9ESVIgfSBmcm9tICcuLi91dGlscy91dGlscyc7XG5pbXBvcnQgKiBhcyBwYXRoIGZyb20gJ3BhdGgnO1xuXG5cbmV4cG9ydCBjbGFzcyBTaWduQXBrQ29tbWFuZCB7XG4gICAgY29tbWFuZCA9IFwic2lnbjphcGtcIjtcbiAgICBkZXNjcmliZSA9IFwic2lnbiB0aGUgYXBrIHdpdGggdGhlIGtleXN0b3JlXCI7XG5cblxuXG5cbiAgICBidWlsZGVyKHlhcmdzOiBhbnkpIHtcbiAgICAgICAgcmV0dXJuIHlhcmdzXG4gICAgICAgICAgICAub3B0aW9uKFwia3NcIiwge1xuICAgICAgICAgICAgICAgIGFsaWFzOiBcImtzTmFtZVwiLFxuICAgICAgICAgICAgICAgIGRlZmF1bHQ6IFwiZGVmYXVsdFwiXG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgLm9wdGlvbihcImFsXCIsIHtcbiAgICAgICAgICAgICAgICBhbGlhczogXCJhbGlhc05hbWVcIixcbiAgICAgICAgICAgICAgICBkZWZhdWx0OiBcImRlZmF1bHRcIlxuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIC5vcHRpb24oXCJwd2RcIiwge1xuICAgICAgICAgICAgICAgIGFsaWFzOiBcInBhc3N3b3JkXCIsXG4gICAgICAgICAgICAgICAgZGVmYXVsdDogXCJkZWZhdWx0XCJcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAub3B0aW9uKFwiYVBcIiwge1xuICAgICAgICAgICAgICAgIGFsaWFzOlwiYXBrUGF0aFwiLFxuICAgICAgICAgICAgICAgIGRlZmF1bHQ6IFwiZGVmYXVsdFwiXG4gICAgICAgICAgICB9KTtcbiAgICB9XG5cbiAgICAvLyBhc3luYyBoYW5kbGVyKGFyZ3Y6IGFueSkge1xuICAgIC8vICAgICB0cnkge1xuICAgIC8vICAgICAgICAgY29uc3QgYXJnczogc3RyaW5nW10gPSBbXTtcbiAgICAvLyAgICAgICAgIGNvbnN0IHNoZWxsUGF0aCA9IHBhdGguam9pbihTSEVMTF9ESVIsICdzaWduLWFway5zaCcpO1xuICAgIC8vICAgICAgICAgYXdhaXQgZXhlY1Rhc2soJ3NoJywgW3NoZWxsUGF0aCwgLi4uYXJnc10pO1xuICAgIC8vICAgICB9IGNhdGNoIChlcnJvcikge1xuICAgIC8vICAgICAgICAgY29uc29sZS5lcnJvcihlcnJvcik7XG5cbiAgICAvLyAgICAgfVxuICAgIC8vIH1cblxuICAgIGFzeW5jIGhhbmRsZXIoYXJndjogYW55KSB7XG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICBjb25zdCBzaGVsbFBhdGggPSBwYXRoLmpvaW4oU0hFTExfRElSLCAnc2lnbi1hcGsuc2gnKTtcbiAgICAgICAgICAgIGF3YWl0IGV4ZWNUYXNrKHNoZWxsUGF0aCwgW2FyZ3Yua3NOYW1lLCBhcmd2LmFsaWFzTmFtZSwgYXJndi5wYXNzd29yZCwgYXJndi5hcGtQYXRoLCBTSUdORVJfRElSXSk7XG4gICAgICAgIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKGVycm9yKTtcbiAgICAgICAgfVxuICAgIH1cbn0iXX0=