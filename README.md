## Hachim-Signer
![Downloads : +300](https://img.shields.io/badge/Downloads-%2B300-brightgreen.svg "Downloads : +300")

![Ionic 3.6.0](https://img.shields.io/badge/Ionic-3.6.0-blue.svg "Ionic Version : 3.6.0")

A simple package for your NodeJs project allowing you to :
  * Create a _key.keystore_ 
  * Sign up your APK either with the key created above or one of your own.

I have used the __uber-apk-signer.jar__ for that.

This package was made for my Ionic projects so it's fully compatible with Ionic 3.6.0

#### Version
___1.07___ 

![Version]( https://img.shields.io/badge/Version-1.7.0-blue.svg "1.7")

#### Installation

> npm install --save-dev hachim-signer@latest


#### Usage

##### create the keystore

> sign keystore:create --ks _key.keystore_ --al _aliasName_ --pwd _yourPassword_

##### Sign your apk

> sign sign:apk --ks _key.keystore_ --al _aliasName_ --pwd _yourPassword_ --aP _path/to/app.apk_


#### Arguments

| Arguments     | Description   |
| ------------- |:-------------:|
| --ks          | keystore Name |
| --al          | Alias Name (Default is : mainKeyStore)      |
| --pwd         | Password for the storepass and keypass      |
| --aP          | the path to the apkto be signed  |

#### Source Code :
Made by *Hachim JABRI* for ***Magnus 2C***